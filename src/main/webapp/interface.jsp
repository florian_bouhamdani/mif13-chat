<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"
import="java.util.*"%>
<%! private String salon;%>
<% salon = (String) request.getParameter("s"); %>
<!DOCTYPE xhtml>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Mon appli de chat</title>
	<link rel="stylesheet" type="text/css" href="css/general.css">
</head>
<body>
	<div id="salons-block">
		<h2><% out.println(salon); %></h2>
	</div>
	<div id="interface">
		<div id="chat">
			<iframe name="messages" src="Messages.jsp?s=<%=salon%>"></iframe>
		</div>
		<div id="send_message">
			<div class="left">
<%-- 				<form id="frm_send_message" name="send_message" method="POST" action="controleur?s=<%=salon%>" target="messages"> --%>
				<form id="frm_send_message" name="send_message" method="POST" action="rest/conversation/<%=salon%>/messages/add" target="messages">
					<ul>
						<li>
							<input type="text" id="message" name="message" placeholder="Entrez votre message..."/>
							<input type="hidden" id="pseudo" name="pseudo" value='<%=session.getAttribute("pseudo")%>'/>
							<input type="submit" id="submit" name="submit" value="Envoyer"/>
						</li>
					</ul>
				</form>
			</div>
			<div class="right">
				<button id="exit" onclick="location.href='exit.html'">Quitter</button>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
	<footer>
		Florian Bouhamdani 11307717 - Roux Martin 11308229 - MIF13
	</footer>
</body>
</html>