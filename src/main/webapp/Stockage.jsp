<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"
import="java.util.*,modele.Message,modele.Salon,modele.GestionSalons"%>
<jsp:useBean id="salons" scope="session" class="modele.GestionSalons" />
<%
	String salon_name = request.getParameter("salon");
	if(salon_name==null) salon_name = (String) session.getAttribute("salon");
	
	//On recherche dans la liste si le salon existe
	int index = -1;
	for(int i=0; i<GestionSalons.getNbSalons(); i++) {
		if(GestionSalons.getSalon(i).getNom().equals(salon_name)) index = i;
	}
	
	//S'il n'existe pas on l'ajoute
	Salon salon=null;
	if(index == -1) {
		GestionSalons.ajouterSalon(salon_name);
		index = GestionSalons.getSalonIndex(salon_name);
	}
	
	if(GestionSalons.getSalon(index) != null) {
		String message = request.getParameter("message");
		if(message != null && !message.equals("")) {
			String pseudo = (String) session.getAttribute("pseudo");
			GestionSalons.getSalon(index).getMessages().ajouterMessage(pseudo,message);
		}
	}
%>