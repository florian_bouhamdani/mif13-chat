<!DOCTYPE xhtml>
<html>
<head>
	<title>Mon appli de chat</title>
	<link rel="stylesheet" type="text/css" href="css/general.css">
</head>
<body id="exit-page">
	<div class="card signin-card">
		<h2 class="small-title center">Version du chat</h2>
		<button class="button" onclick="location.href='rest/conversation/<%=session.getAttribute("salon")%>'">Version basique</button>
		<button class="button" onclick="location.href='interfaceAJAX.jsp'">Version AJAX</button>
	</div>
	<footer>
		<div>Florian Bouhamdani 11307717 - Roux Martin 11308229 - MIF13</div>
	</footer>
</body>
</html>