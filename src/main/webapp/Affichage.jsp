<%@page import="modele.GestionMessages"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"
import="java.util.*,modele.Message,modele.Salon,modele.GestionSalons"%>
<jsp:useBean id="bean_salons" scope="session" type="modele.GestionSalons" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Chat</title>
	<link rel="stylesheet" type="text/css" href="css/general.css">
</head>
<body>
	<div id="messages">
		<ul>
			<%
			//On r�cup�re le nom du salon
			String salon_name = request.getParameter("s");
			System.out.println("sal : "+salon_name);
			
			//On recherche dans la liste si le salon existe
			int index = GestionSalons.getSalonIndex(salon_name);
			
			//on affiche les messages de ce salon
			if(index!=-1 && GestionSalons.getSalon(index)!=null && GestionSalons.getSalon(index).getMessages()!=null 
				&& GestionSalons.getSalon(index).getMessages().getNbMessages()>0) 
			{
				for(int i=0; i<GestionSalons.getSalon(index).getMessages().getNbMessages(); i++) {
					Message msg = GestionSalons.getSalon(index).getMessages().getMessage(i);
					out.println("<li class='msg'>["+msg.getDate()+"] "+msg.getExpediteur()+" : "+msg.getMessage()+"</li>");
				}
			}
			%>
		</ul>
	</div>
</body>
</html>