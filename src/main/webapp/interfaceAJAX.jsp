<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Chat - AJAX</title>
	<script type="text/javascript" src="js/biblio_ajax.js"></script>
	<link rel="stylesheet" href="css/general.css"/>
	<script>
		function validation() {
			var pseudo = document.forms["frm_send_message"]["pseudo"].value;
			var message = document.forms["frm_send_message"]["message"].value;
			var salon = document.forms["frm_send_message"]["salon"].value;
			var params = "pseudo="+pseudo+"&message="+message+"&salon="+salon;
				
			if(message != "") {
				sendRequestAsynchroneously('post','/Chat/rest/conversation/'+salon+'/messages/add',params);
				loadXMLAsynchroneously('GET','/Chat/rest/conversation/'+salon+'/messages/-1',null,'messages');
			}

			return false;
		}
	</script>
</head>
<body id="ajax" onload="boucle('<%=session.getAttribute("salon")%>');">
	<div id="salons-block">
		<h2><%=session.getAttribute("salon")%></h2>
	</div>
	<div id="interface">
		<div id="chat">
			<div id="messages"></div>
		</div>
		<div id="send_message">
			<div class="left">	
				<form id="frm_send_message" onsubmit="return validation()">
					<ul>
						<li>
							<input type="text" id="message" name="message"/>
							<input type="hidden" id="pseudo" name="pseudo" value='<%=session.getAttribute("pseudo")%>'/>
							<input type="hidden" id="salon" name="salon" value='<%=session.getAttribute("salon")%>'/>
							<input type="submit" id="submit" name="submit" value="Envoyer"/>
						</li>
					</ul>
				</form>
			</div>
			<div class="right">
				<button id="exit" onclick="location.href='/Chat/exit.jsp'">Quitter</button>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
</body>
</html>