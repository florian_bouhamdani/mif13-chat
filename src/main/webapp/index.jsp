<!DOCTYPE xhtml>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Mon appli de chat</title>
	<link rel="stylesheet" type="text/css" href="css/general.css">
</head>
<body>
<div id="body-connexion">
	<h1 class="center">Bienvenue dans mon appli de chat</h1>
	
	<div class="card signin-card">
		<h2 class="small-title center">Connectez-vous</h2>
		<form id="connexion" name="connexion" method="POST" action="init">
			<ul>
				<li>
					<input type="text" id="pseudo" name="pseudo" placeholder="Pseudo"/>
					<input type="text" id="salon" name="salon" placeholder="Nom du salon"/>
					<input type="submit" id="submit" name="submit" value="Connexion"/>
				</li>
			</ul>
		</form>
	</div>
</div>
<footer>
		<div>Florian Bouhamdani 11307717 - Roux Martin 11308229 - MIF13</div>
	</footer>
</body>
</html>