<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%response.addHeader("Refresh","5");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Chat</title>
	<style>
		ul {list-style-type:none; margin:0; padding:0;}
		.msg {padding:8px 0px;}
	</style>
</head>
<body>
	<div id="messages">
		<ul>
			<c:forEach var="item" items="${it.messages}">
		        <li class='msg'>[${item.date}] ${item.expediteur} : ${item.message}</li>
		    </c:forEach>
		</ul>
	</div>
</body>
</html>