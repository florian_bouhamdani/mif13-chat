<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<ul>
			<xsl:apply-templates/>
		</ul>
	</xsl:template>
	
	<xsl:template match="messages/message">
		<li>
			[<xsl:value-of select="date"/>] <xsl:value-of select="expediteur"/> :
				<xsl:value-of select="message"/>
		</li>
	</xsl:template>
</xsl:stylesheet>