package controleur;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modele.GestionSalons;

/**
 * Servlet implementation class Controleur
 */
public class Controleur extends HttpServlet {
	
	//public GestionSalons bean_salons = new GestionSalons();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controleur() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response); 
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
		
	    response.setContentType("text/html;charset=UTF-8");
	    PrintWriter out = response.getWriter();
	    try {
	    	HttpSession session = request.getSession();
			String pseudo = (String) session.getAttribute("pseudo");
			String message = request.getParameter("message");
			String salon_name = request.getParameter("s");
			
			if(pseudo!=null && message!=null && salon_name!=null) {
				//SET VALUES TO BEAN
				//On recherche dans la liste si le salon existe
				int index = -1;
				for(int i=0; i<GestionSalons.getNbSalons(); i++) {
					if(GestionSalons.getSalon(i).getNom().equals(salon_name)) index = i;
				}
				
				//S'il n'existe pas on l'ajoute
				if(index == -1) {
					GestionSalons.ajouterSalon(salon_name);
					index = GestionSalons.getSalonIndex(salon_name);
				}
				
				//On ajoute le message
				if(GestionSalons.getSalon(index) != null) {
					if(message != null && !message.equals("")) {
						GestionSalons.getSalon(index).getMessages().ajouterMessage(pseudo,message);
					}
				}

				//session.setAttribute("bean_salons",bean_salons);
                RequestDispatcher rd= request.getRequestDispatcher("Messages.jsp?s="+salon_name);
                rd.forward(request,response);
			}
		} catch(Exception e) {
			response.sendRedirect("index.jsp");
		} finally {
			out.close();
		}
	}
}
