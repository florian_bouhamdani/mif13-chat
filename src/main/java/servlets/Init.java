package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modele.GestionSalons;

/**
 * Servlet implementation class Init
 */
public class Init extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Init() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			//1. cr�ation / r�cup�ration de la session de l'utilisateur
			HttpSession session = request.getSession(true);  
			
			//2. r�cup�ration du param�tre de la requ�te contenant le pseudo saisi par l'utilisateur
			String pseudo = request.getParameter("pseudo");
			String salon = request.getParameter("salon");
			
			if(pseudo==null || salon==null || pseudo.isEmpty() || salon.isEmpty()) {
				throw new Exception();
			}
			//3. cr�ation d'un attribut de la session ayant pour valeur ce pseudo
			session.setAttribute("pseudo",pseudo);
			session.setAttribute("salon",salon);
			request.setAttribute("sl",salon);
			
			//4. redirection vers interface.html
			if(GestionSalons.getSalonIndex(salon) == -1) GestionSalons.ajouterSalon(salon);
			//response.sendRedirect("rest/conversation/"+salon);
			response.sendRedirect("version.jsp");
			
			//session.setAttribute("bean_salons",new GestionSalons());
		} catch(Exception e) {
			response.sendRedirect("index.jsp");
		}
	}

}
