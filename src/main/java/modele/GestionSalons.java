package modele;

import java.util.ArrayList;
import java.util.List;

public class GestionSalons {

	private static List<Salon> salons = new ArrayList<Salon>();

	public GestionSalons(List<Salon> salons) {
		super();
		this.setSalons(salons);
	}
	
	public GestionSalons() {
		this.setSalons(new ArrayList<Salon>());
	}

	public static void ajouterSalon(String nom) {
		GestionSalons.salons.add(new Salon(nom));
	}
	
	public static Salon getSalon(int index) {
		return GestionSalons.salons.get(index);
	}
	
	public static int getSalonIndex(String name) {
		for(int i=0; i<salons.size(); i++) {
			if(salons.get(i).getNom().equals(name)) return i;
		}
		return -1;
	}
	
	public static Salon get(String name) {
		if(salons==null) return null;
		
		for(int i=0; i<salons.size(); i++) {
			if(salons.get(i).getNom().equals(name)) return salons.get(i);
		}
		return null;
	}
 	
	public static int getNbSalons() {
		return GestionSalons.getSalons().size();
	}
	
	//Getters & setters
	public static List<Salon> getSalons() {
		return salons;
	}

	public void setSalons(List<Salon> salons) {
		GestionSalons.salons = salons;
	}
}
