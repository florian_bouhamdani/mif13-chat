package modele;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GestionMessages {

	private List<Message> messages;

	public GestionMessages(List<Message> messages) {
		super();
		this.setMessages(messages);
	}
	
	public GestionMessages() {
		this.setMessages(new ArrayList<Message>());
	}

	public void ajouterMessage(String pseudo, String msg) {
		int id = messages.size();
		this.messages.add(new Message(id,pseudo,msg));
	}
	
	public void supprimerMessage(int id) {
		this.messages.remove(id);
	}
	
	public Message getMessage(int index) {
		return this.messages.get(index);
	}
 	
	public int getNbMessages() {
		return this.getMessages().size();
	}
	
	//Getters & setters
	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
}
