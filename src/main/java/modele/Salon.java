package modele;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Salon {
	private String nom;
	private GestionMessages messages;
	
	public Salon(String nom, GestionMessages messages) {
		super();
		this.setNom(nom);
		this.setMessages(messages);
	}
	
	public Salon(String nom) {
		super();
		this.setNom(nom);
		this.setMessages(new GestionMessages());
	}
	
	protected Salon() {}

	//Setters && getters
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public GestionMessages getMessages() {
		return messages;
	}

	public void setMessages(GestionMessages messages) {
		this.messages = messages;
	}

	/*
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Salon other = (Salon) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}
	*/
}
