package ressources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import modele.GestionSalons;
import modele.Message;

@Path("/message")
public class MessageRessource {
 
	//R�cup�re les infos d'un message � partir du nom du salon et de l'id du msg
	//Renvoie du XML si le client accepte
	@GET
	@Path("/{name}/{ident}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getMessageInfoAsXml(@PathParam("name") String name,@PathParam("ident") int ident) {
		Message msg = GestionSalons.get(name).getMessages().getMessage(ident);
		return Response.ok(msg).build();
	}
	
	//R�cup�re les infos d'un message � partir du nom du salon et de l'id du msg
	//Sinon renvoie du Json
	@GET
	@Path("/{name}/{ident}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessageInfoAsJson(@PathParam("name") String name,@PathParam("ident") int ident) {
		Message msg = GestionSalons.get(name).getMessages().getMessage(ident);
		return Response.ok(msg).build();
	}
		
		
		
	//Modifie le contenu du dernier message d'une conversation 
	//(doit renvoyer une erreur si ce n'est pas le dernier message)
	@POST
	@Path("/{name}/{ident}/update")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateLastMessage(@PathParam("name") String name,
			@PathParam("ident") int ident,@FormParam("message") String message) {
		
		//Renvoie une erreur si ce n'est pas le dernier message
		if(GestionSalons.get(name).getMessages().getNbMessages()-1 != ident) {
			return Response.status(406).build();
		}
		
		GestionSalons.get(name).getMessages().getMessage(ident).setMessage(message);
		Message msg = GestionSalons.get(name).getMessages().getMessage(ident);
		return Response.ok(msg).build();
	}
	
	//Supprime le dernier message d'une conversation 
	//(doit renvoyer une erreur si ce n'est pas le dernier message)
	@DELETE
	@Path("/{name}/{ident}/delete")
	public Response deleteLastMessage(@PathParam("name") String name,@PathParam("ident") int ident) {
		
		//Renvoie une erreur si ce n'est pas le dernier message
		if(GestionSalons.get(name).getMessages().getNbMessages()-1 != ident) {
			return Response.status(406).build();
		}
		
		GestionSalons.get(name).getMessages().supprimerMessage(ident);
		return Response.ok("Message supprim�").build();
	}
}
