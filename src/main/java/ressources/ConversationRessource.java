package ressources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.view.Viewable;

import modele.GestionMessages;
import modele.GestionSalons;
import modele.Message;
import modele.Salon;

@Path("/conversation")
public class ConversationRessource {
 
	//R�cup�re le salon
	//Renvoie du XML si le client accepte
	@GET
	@Path("/{name}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getConversationAsXml(@PathParam("name") String name) {
		if(GestionSalons.getSalonIndex(name)==-1) return Response.status(404).build();
		Salon salon = GestionSalons.get(name);
		return Response.ok(new Viewable("/interfaceRest",salon)).build();
	}
	
	//R�cup�re le salon
	//Sinon du JSON
	@GET
	@Path("/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConversationAsJson(@PathParam("name") String name) {
		if(GestionSalons.getSalonIndex(name)==-1) return Response.status(404).build();
		Salon salon = GestionSalons.get(name);
		return Response.ok(new Viewable("/interfaceRest",salon)).build();
	}
	
	//R�cup�re le nombre de message du salon
	@GET
	@Path("/{name}/messages/count")
	public Response getNbMsgAsXml(@PathParam("name") String name) {
		int nb = GestionSalons.get(name).getMessages().getNbMessages();
		return Response.ok(""+nb).build();
	}

	//R�cup�re tous les messages
	//Renvoie du XML
	@GET
	@Path("/{name}/messages")
	@Produces(MediaType.APPLICATION_XML)
	public Response getMessagesAsXml(@PathParam("name") String name) {
		if(GestionSalons.getSalonIndex(name)==-1) return Response.status(404).build();
		GestionMessages messages = GestionSalons.get(name).getMessages();
		return Response.ok(new Viewable("/affichageRest",messages)).build();
	}
	
	//R�cup�re tous les messages
	//Renvoie du Json
	@GET
	@Path("/{name}/messages")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessagesAsJson(@PathParam("name") String name) {
		GestionMessages messages = GestionSalons.get(name).getMessages();
		return Response.ok(new Viewable("/affichageRest",messages)).build();
	}
	
	//R�cup�re tous les messages envoy�s apr�s un message donn�, dont l'id sera pass� en param�tre
	//Renvoie du XML
	@GET
	@Path("/{name}/messages/{ident}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getLastMessagesAsXml(@PathParam("name") String name,@PathParam("ident") int ident) {
		List<Message> messages = new ArrayList<Message>();
		GestionMessages list = GestionSalons.get(name).getMessages();
		ident++;
		while(ident < list.getNbMessages()) {
			messages.add(list.getMessage(ident));
			ident++;
		}
		GenericEntity<List<Message>> entity = new GenericEntity<List<Message>>(messages) {};
		return Response.ok(entity).build();
	}
	
	//R�cup�re tous les messages envoy�s apr�s un message donn�, dont l'id sera pass� en param�tre
	//Renvoie du Json
	/*
	@GET
	@Path("/{name}/messages/{ident}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLastMessagesAsJson(@PathParam("name") String name,@PathParam("ident") int ident) {
		List<Message> messages = new ArrayList<Message>();
		GestionMessages list = GestionSalons.get(name).getMessages();
		ident++;
		while(ident < list.getNbMessages()) {
			messages.add(list.getMessage(ident));
			ident++;
		}
		GenericEntity<List<Message>> entity = new GenericEntity<List<Message>>(messages) {};
		return Response.ok(entity).build();
	}
	*/
	
	//Ajoute un message depuis le formulaire d'envoie
	@POST
	@Path("/{name}/messages/add")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void addMessage(@PathParam("name") String name,
						@FormParam("message") String message,
						@FormParam("pseudo") String pseudo,
						@Context HttpServletResponse servletResponse) throws IOException {

		if(GestionSalons.get(name)==null) GestionSalons.ajouterSalon(name);
		GestionSalons.get(name).getMessages().ajouterMessage(pseudo,message);
		servletResponse.sendRedirect("/Chat/rest/conversation/"+name+"/messages/");
	}
}
