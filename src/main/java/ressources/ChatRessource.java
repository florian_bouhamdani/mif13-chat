package ressources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import modele.GestionSalons;
import modele.Salon;

@Path("/chat")
public class ChatRessource {
 
	//R�cup�re l'ensemble des conversations existantes
	//Renvoie du XML si le client accepte
	@GET
	@Produces(MediaType.APPLICATION_XML)
    public Response getAsXml() {
		List<Salon> salons = GestionSalons.getSalons();
	    GenericEntity<List<Salon>> entity = new GenericEntity<List<Salon>>(salons){};      
	    return Response.ok(entity).build();
	}
		
	//R�cup�re l'ensemble des conversations existantes
	//Sinon renvoie du Json
	@GET
	@Produces(MediaType.APPLICATION_JSON)
    public Response getAsJson() {
		List<Salon> salons = GestionSalons.getSalons();
	    GenericEntity<List<Salon>> entity = new GenericEntity<List<Salon>>(salons){};      
	    return Response.ok(entity).build();
	}
	
	
	//R�cup�re le nombre de message du salon
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response addSalon(@FormParam("salon") String salon) {
		GestionSalons.ajouterSalon(salon);
		return Response.ok(GestionSalons.get(salon)).build();
	}	
}
